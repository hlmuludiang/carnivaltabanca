//
//  NewCell.h
//  CarnivalTabanca
//
//  Created by Versatile Systems, Inc on 11/9/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import <Parse/Parse.h>
#import "CarnivalTabancaData.h"
#import "TPFloatRatingView.h"

@interface CarnivalsTableViewCell : PFTableViewCell <UIAlertViewDelegate, UINavigationControllerDelegate, TPFloatRatingViewDelegate,STABannerDelegateProtocol> {
    CarnivalTabancaData *_carnivalTabancaData;
    IBOutlet TPFloatRatingView *_starRating;
    IBOutlet UIButton *_thumbsUp;
    IBOutlet UIButton *_thumbsDown;
    int _currentVote;
    NSString *_voteId;
    IBOutlet UILabel *_voterCount;
    IBOutlet UILabel *_monthLabel;
    IBOutlet UILabel *_dayLabel;
    IBOutlet UIView *_calendarMask;
}

@property (weak,nonatomic) IBOutlet UILabel *cellTitle;
@property (weak, nonatomic) IBOutlet UILabel *cellSubTitle;
@property (retain, nonatomic) IBOutlet UIImageView *flagBackground;
@property (weak, nonatomic) NSString *carnivalId;

-(void)showStars:(float)votes voters:(float)voters;
-(void)setCalendar:(NSString *)month day:(NSString *)day;

@end

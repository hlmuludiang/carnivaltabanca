//
//  SocialTableViewCell.m
//  CarnivalTabanca
//
//  Created by Versatile Systems, Inc on 11/23/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import "SocialTableViewCell.h"

@implementation SocialTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setProfile:(NSString *)profile image:(UIImage *)image buttonTag:(NSInteger)tag{
    _carnivalTabancaData = [CarnivalTabancaData sharedData];
    [_profileButton setTitle:profile forState:UIControlStateNormal];
    [_profileButton setBackgroundImage:[_carnivalTabancaData button:@"tan"] forState:UIControlStateNormal];
    [_profileButton setTag:tag];
    [_profileImage setImage:image];
    //NSLog(@"Profile set");
}

@end

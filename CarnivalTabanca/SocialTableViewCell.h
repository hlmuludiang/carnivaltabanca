//
//  SocialTableViewCell.h
//  CarnivalTabanca
//
//  Created by Versatile Systems, Inc on 11/23/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CarnivalTabancaData.h"

@interface SocialTableViewCell : UITableViewCell {
    CarnivalTabancaData *_carnivalTabancaData;
    IBOutlet UIImageView *_profileImage;
    IBOutlet UIButton *_profileButton;
}

-(void)setProfile:(NSString *)profile image:(UIImage *)image buttonTag:(NSInteger)tag;

@end

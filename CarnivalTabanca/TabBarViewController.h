//
//  TabBarViewController.h
//  myCarnivalDiary
//
//  Created by Versatile Systems, Inc on 10/27/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CarnivalTabancaData.h"
#import "CarnivalsTableViewController.h"

@interface TabBarViewController : UITabBarController {
        IBOutlet UIBarButtonItem *_loginButton;
    CarnivalTabancaData *_carnivalTabancaData;
}

@end

//
//  CarnivalPOIViewController.h
//  CarnivalTabanca
//
//  Created by Versatile Systems, Inc on 11/17/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CarnivalTabancaData.h"
#import <CoreLocation/CoreLocation.h>
#import "CarnivalTabancaData.h"

@interface CarnivalPOIViewController : UIViewController <UIPickerViewDelegate, UIPickerViewDataSource, UIAlertViewDelegate, UITextFieldDelegate, CLLocationManagerDelegate> {
    CarnivalTabancaData *_carnivalDiaryData;
    UIPickerView *_POITypes;
    IBOutlet UITextField *_location;
    IBOutlet UITextView *_instructions;
    IBOutlet UITextField *_locationAddress;
    NSArray *_pickerOptions;
    int _typePicked;
    IBOutlet UITextField *_venueType;
    IBOutlet UIButton *_getLocationButton;
    CLLocationManager *_locationManager;
    CLGeocoder *_geoCoder;
    CLPlacemark *_placemark;
    IBOutlet UIButton *_dismissKeyboardButton;
}

@property (nonatomic,weak) NSString *carnivalId;
@property (nonatomic, weak) NSString *carnivalName;
@end

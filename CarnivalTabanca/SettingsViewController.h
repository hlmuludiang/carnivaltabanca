//
//  SecondViewController.h
//  myCarnivalDiary
//
//  Created by Hilary Muludiang on 10/26/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CarnivalTabancaData.h"
#import "AuthenticateUserViewController.h"
#import "SubmitToJIRAViewController.h"
#import "SocialTableViewCell.h"
#import "ContactTableViewCell.h"
#import "DataTableViewCell.h"
#import "CarnivalWebsiteViewController.h"
#import <MessageUI/MessageUI.h>

@interface SettingsViewController : UIViewController <UIActionSheetDelegate, UITableViewDataSource, UITableViewDelegate, MFMailComposeViewControllerDelegate >{
    CarnivalTabancaData *_carnivalTabancaData;
    SocialTableViewCell *_socialCell;
    ContactTableViewCell *_contactCell;
    DataTableViewCell *_dataCell;
    IBOutlet UIButton *_loginLogoutButton;
    IBOutlet UILabel *_dataLabel;
    UIBarButtonItem *_llButton;
    IBOutlet UIButton *_linkToSocialNetworkButton;
    IBOutlet UIImageView *_socialNetworkProfileImage;
    UIButton *_customButton;
    IBOutlet UIButton *_actionButton;;
    IBOutlet UITableView *_tableView;
    NSArray *_tableData;
}


@end


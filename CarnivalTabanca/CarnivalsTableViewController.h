//
//  NewCarnivalsTableViewController.h
//  CarnivalTabanca
//
//  Created by Versatile Systems, Inc on 11/9/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import <Parse/Parse.h>
#import "CarnivalTabancaData.h"
#import "CarnivalDetailsViewController.h"
#import "CarnivalsTableViewCell.h"
#import "CustomLoginViewController.h"
#import "CustomSignUpViewController.h"
#import "SubmitToJIRAViewController.h"

@interface CarnivalsTableViewController : PFQueryTableViewController <PFLogInViewControllerDelegate, PFSignUpViewControllerDelegate, UITableViewDataSource, UITableViewDelegate>{
    CarnivalTabancaData *_carnivalTabancaData;
    CustomLoginViewController *_logInController;
    CustomSignUpViewController *_signUpController;
}

@end

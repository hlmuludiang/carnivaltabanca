//
//  ContactTableViewCell.h
//  CarnivalTabanca
//
//  Created by Versatile Systems, Inc on 11/23/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CarnivalTabancaData.h"

@interface ContactTableViewCell : UITableViewCell{
    CarnivalTabancaData *_carnivalTabancaData;
    IBOutlet UIButton *_contactButton;
}

-(void)setContact:(NSString *)title buttonTag:(NSInteger)tag;

@end

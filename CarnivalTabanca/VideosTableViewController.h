//
//  NewVideosViewController.h
//  CarnivalTabanca
//
//  Created by Versatile Systems, Inc on 11/10/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import <Parse/Parse.h>
#import "CarnivalTabancaData.h"
#import "VideosTableViewCell.h"

@interface VideosTableViewController : PFQueryTableViewController <UITableViewDataSource, UITableViewDelegate, STABannerDelegateProtocol >{
    CarnivalTabancaData *_carnivalTabancaData;
}

@end

//
//  ContactTableViewCell.m
//  CarnivalTabanca
//
//  Created by Versatile Systems, Inc on 11/23/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import "ContactTableViewCell.h"

@implementation ContactTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setContact:(NSString *)title buttonTag:(NSInteger)tag{
    _carnivalTabancaData = [CarnivalTabancaData sharedData];
    [_contactButton setBackgroundImage:[_carnivalTabancaData button:@"tan"] forState:UIControlStateNormal];
    [_contactButton setTitle:title forState:UIControlStateNormal];
    [_contactButton setTag:tag];
}
@end

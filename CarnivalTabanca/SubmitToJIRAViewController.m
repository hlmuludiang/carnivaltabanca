//
//  SubmitToJIRAViewController.m
//  CarnivalTabanca
//
//  Created by Versatile Systems, Inc on 11/11/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import "SubmitToJIRAViewController.h"

@interface SubmitToJIRAViewController ()

@end

@implementation SubmitToJIRAViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSLog(@"Issue Type: %@", _issueType);
    _carnivalTabancaData = [CarnivalTabancaData sharedData];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(submitToJIRA)];
    _details.layer.borderWidth = 2.0f;
    _details.layer.borderColor = [UIColor grayColor].CGColor;
    _description.layer.borderWidth = 2.0f;
    _description.layer.borderColor = [UIColor grayColor].CGColor;
    _details.delegate = self;
    _description.delegate = self;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [_details becomeFirstResponder];
    return YES;
}

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    [self.view setFrame:CGRectMake(0, -50, self.view.frame.size.width, self.view.frame.size.height)];
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
            [self.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        return NO;
    }
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)dismissKeyboard:(id)sender{
    [self.view endEditing:YES];
    [self.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
}

-(void)submitToJIRA{
    //NSLog(@"Submitting issue to JIRA");
    if ([_description.text isEqualToString:@""] || [_details.text isEqualToString:@""]){
        UIAlertView *submitAlert = [[UIAlertView alloc] initWithTitle:@"Form incomplete" message:@"Please fill out the short description and details fields." delegate:self cancelButtonTitle:@"Okay" otherButtonTitles: nil];
        [submitAlert show];
    } else {
    [_carnivalTabancaData submitIssue:_description.text content:_details.text type:_issueType];
    [self.navigationController popViewControllerAnimated:YES];
}
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

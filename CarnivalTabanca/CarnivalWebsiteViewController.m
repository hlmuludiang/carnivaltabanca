//
//  CarnivalWebsiteViewController.m
//  CarnivalTabanca
//
//  Created by Versatile Systems, Inc on 11/15/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import "CarnivalWebsiteViewController.h"

@interface CarnivalWebsiteViewController ()

@end

@implementation CarnivalWebsiteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Close" style:UIBarButtonItemStyleDone target:self action:@selector(dismissView:)];
    [_carnivalWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:_websiteURL]]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)dismissView:(UIBarButtonItem *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

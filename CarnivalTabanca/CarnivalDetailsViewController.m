//
//  CarnivalDetailsViewController.m
//  myCarnivalDiary
//
//  Created by Hilary Muludiang on 10/26/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import "CarnivalDetailsViewController.h"

#define METERS_PER_MILE 1609.334

@interface CarnivalDetailsViewController ()

@end

@implementation CarnivalDetailsViewController

-(BOOL)prefersStatusBarHidden{
    return NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(addVenuesToMap)
                                                 name:@"addVenues"
                                               object:nil];
    _annotationsArray = [[NSMutableArray alloc] init];
    // Do any additional setup after loading the view.
    _carnivalDairyData = [CarnivalTabancaData sharedData];
    //self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(composeMessage)];
    if ([_carnivalObject valueForKey:@"Name"]){
        self.navigationItem.title = [_carnivalObject valueForKey:@"Name"];
    } else if([_carnivalObject valueForKey:@"City"]) {
        self.navigationItem.title = [NSString stringWithFormat:@"%@ Carnival", [_carnivalObject valueForKey:@"City"]];
    } else {
        self.navigationItem.title =[NSString stringWithFormat:@"%@ Carnival", [_carnivalObject valueForKey:@"Country"]] ;
    }
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSLocale *enUSPOSIXLocale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormatter setLocale:enUSPOSIXLocale];
    [dateFormatter setDateFormat:@"EEE., MMM dd yyyy"];
    if ([_carnivalObject valueForKey:@"End"] != NULL){
        _carnivalDates = [NSString stringWithFormat:@"%@ - %@", [dateFormatter stringFromDate:[_carnivalObject valueForKey:@"Start"]], [dateFormatter stringFromDate:[_carnivalObject valueForKey:@"End"]]
                               ];
    } else {
        _carnivalDates = [NSString stringWithFormat:@"%@ - %@", [dateFormatter stringFromDate:[_carnivalObject valueForKey:@"Start"]], [dateFormatter stringFromDate:[_carnivalObject valueForKey:@"Start"]]
                               ];
    }
    
    self.navigationItem.prompt = _carnivalDates;
    [self getExchangeRate];
    
    
    
    if ([_carnivalObject valueForKey:@"City" ] != NULL){
        [self generateMapPoints:[NSString stringWithFormat:@"%@, %@", [_carnivalObject valueForKey:@"City"], [_carnivalObject valueForKey:@"Country"]] type:[_carnivalObject valueForKey:@"City"]];
    } else {
        [self generateMapPoints:[_carnivalObject valueForKey:@"Country"] type:[_carnivalObject valueForKey:@"Country"]];
    }
    
    PFObject *currentCarnival = [PFObject objectWithoutDataWithClassName:@"Carnivals" objectId:[_carnivalObject valueForKey:@"objectId"]];
    PFRelation *carnivalVenues = [currentCarnival relationForKey:@"Venues"];
    [[carnivalVenues query] findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (error){
            NSLog(@"Error getting venues: %@", error);
            
        } else {
            if ([objects count] > 0) {
                _venues = [[NSArray alloc] initWithArray:objects];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"addVenues" object:nil];
                //NSLog(@"Got venues: %@", _venues);
            }
        }
    }];
}

-(void)styleNavigationBar{
    
    //NSString* navigationTitleFont = @"Avenir-Heavy";
    //UIColor* color = [UIColor colorWithRed:58.0/255 green:153.0/255 blue:216.0/255 alpha:1.0];
    
    NSString* navigationTitleFont = @"EuphemiaUCAS-Bold";
    UIColor* color = [UIColor colorWithRed:216.0/255 green:58.0/255 blue:58.0/255 alpha:1.0];
    
    self.navigationController.navigationBar.barTintColor = color;
    
    self.navigationController.navigationBar.titleTextAttributes =
    
    @{NSForegroundColorAttributeName : [UIColor whiteColor],
      NSFontAttributeName : [UIFont fontWithName:navigationTitleFont size:18.0f]
      };
    
    
    /*UIImageView* searchView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"search.png"]];
     searchView.frame = CGRectMake(0, 0, 20, 20);
     
     UIBarButtonItem* searchItem = [[UIBarButtonItem alloc] initWithCustomView:searchView];
     
     self.navigationItem.rightBarButtonItem = searchItem;*/
     
     UIButton* menuButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 28, 20)];
     [menuButton setImage:[UIImage imageNamed:@"menu"] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(composeMessage) forControlEvents:UIControlEventTouchUpInside];
     
     UIBarButtonItem* menuItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
     self.navigationItem.rightBarButtonItem = menuItem;
}

-(IBAction)dismissView:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)addVenuesToMap{
    //NSLog(@"Ready to add venues: %@" , _venues);
    
    for (int x = 0 ; x < [_venues count]; x++) {
        if ([_venues[x] valueForKey:@"Active"] == [NSNumber numberWithBool:YES]){
            CLGeocoder *geocoder = [[CLGeocoder alloc] init];
            NSString *mapLocation;
            if ([[_venues[x] valueForKey:@"Address"] isEqualToString:@""]){
                mapLocation = [_venues[x] valueForKey:@"Location"];
            } else {
                mapLocation = [NSString stringWithFormat:@"%@, %@", [_venues[x] valueForKey:@"Location"],[_venues[x] valueForKey:@"Address"]];
            }
            
            [geocoder geocodeAddressString:mapLocation completionHandler:^(NSArray *placemarks, NSError *error) {
                if (error){
                    NSLog(@"%@", error);
                } else {
                    //NSLog(@"Adding POI: %@" );
                    CLPlacemark *placeMark = [placemarks lastObject];
                    
                    MKPointAnnotation *pointAnnotation = [[MKPointAnnotation alloc] init];
                    pointAnnotation.coordinate = placeMark.location.coordinate;
                    
                    pointAnnotation.title = [_venues[x] valueForKey:@"Location"];
                    pointAnnotation.subtitle = [_venues[x] valueForKey:@"Type"];
                    
                    //[_mapView addAnnotation:pointAnnotation];
                    
                    [_annotationsArray addObject:pointAnnotation];
                    [_mapView addAnnotations:_annotationsArray];
                    //NSLog(@"Array: %@", _annotationsArray);
                    [_mapView showAnnotations:_annotationsArray animated:YES];
                    
                }
            }];
        }
    }
}

-(void)generateMapPoints:(NSString *)carnivalLocationString type:(NSString *)type{
    //NSLog(@"Map data %@ : %@", carnivalLocationString, type);
    
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder geocodeAddressString:carnivalLocationString completionHandler:^(NSArray *placemarks, NSError *error) {
        if (error){
            NSLog(@"%@", error);
        } else {
            //NSLog(@"Placemark: %@", placemarks);
            CLPlacemark *placeMark = [placemarks lastObject];
            MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(placeMark.location.coordinate, 2*METERS_PER_MILE, 2*METERS_PER_MILE);
            //float spanX = 0.00725;
            //float spanY = 0.00725;
            //region.center.latitude = placeMark.location.coordinate.latitude;
            //region.center.longitude = placeMark.location.coordinate.longitude;
            //region.span = MKCoordinateSpanMake(spanX, spanY);
            
            MKPointAnnotation *pointAnnotation = [[MKPointAnnotation alloc] init];
            pointAnnotation.coordinate = placeMark.location.coordinate;
            pointAnnotation.title = type;
            
            [_mapView addAnnotation:pointAnnotation];
            
            [_mapView setRegion:region animated:YES];
            [_annotationsArray addObject:pointAnnotation];
            //[_mapView showAnnotations:_annotationsArray animated:YES];
        }
    }];
}

- (MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:(id <MKAnnotation>)annotation {
    
    // Don't mess with user location
    if(![annotation isKindOfClass:[ZSAnnotation class]])
        return nil;
    
    ZSAnnotation *a = (ZSAnnotation *)annotation;
    static NSString *defaultPinID = @"StandardIdentifier";
    
    // Create the ZSPinAnnotation object and reuse it
    ZSPinAnnotation *pinView = (ZSPinAnnotation *)[_mapView dequeueReusableAnnotationViewWithIdentifier:defaultPinID];
    if (pinView == nil){
        pinView = [[ZSPinAnnotation alloc] initWithAnnotation:annotation reuseIdentifier:defaultPinID];
    }
    
    // Set the type of pin to draw and the color
    pinView.annotationType = ZSPinAnnotationTypeDisc;
    pinView.annotationColor = a.color;
    pinView.canShowCallout = YES;
    
    return pinView;
    
}

-(void)showExchangeRate{
    UIAlertView *exchangeView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%@ Exchange Rate", _toCurrency] message:[NSString stringWithFormat:@"%@\n%@ 1.00 = %@ %@\nLast updated: %@",[_currencyDictionary valueForKey:@"Name"],_fromCurrencySign, _toCurrencySign, [_currencyDictionary valueForKey:@"Rate"],[_currencyDictionary valueForKey:@"Time"] ] delegate:self cancelButtonTitle:@"Okay" otherButtonTitles: nil];
    [exchangeView show];
}

-(void)gotoWebsite{
    if ([_carnivalObject valueForKey:@"Website"] != NULL){
        //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:[_carnivalObject valueForKey:@"Website"]]];
        CarnivalWebsiteViewController *carnivalWebsite = [self.storyboard instantiateViewControllerWithIdentifier:@"Carnival_website_vc"];
        carnivalWebsite.websiteURL = [_carnivalObject valueForKey:@"Website"];
        [self.navigationController pushViewController:carnivalWebsite animated:YES];
    } else {
        UIAlertView *noWebsite = [[UIAlertView alloc] initWithTitle:@"Error detected" message:@"Sorry. No website listed for this carnival." delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil];
        [noWebsite show];
    }
}

-(void)emailCommittee{
    if ([_carnivalObject valueForKey:@"CommitteeEmail"] != NULL){
        NSArray *emailRecipient = [NSArray arrayWithObjects:[_carnivalObject valueForKey:@"CommitteeEmail"], nil];
        MFMailComposeViewController *composeEmail = [[MFMailComposeViewController alloc] init];
        composeEmail.mailComposeDelegate = self;
        [composeEmail setSubject:[NSString stringWithFormat:@"Inquiry regarding: %@",[_carnivalObject valueForKey:@"Name"] ]];
        [composeEmail setToRecipients:emailRecipient];
        [self presentViewController:composeEmail animated:YES completion:NULL];
    } else {
        UIAlertView *noEmail = [[UIAlertView alloc] initWithTitle:@"Error detected" message:@"Sorry. No email address listed for this carnival committee." delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil];
        [noEmail show];
    }
}

-(void)callCommittee{
    if ([_carnivalObject valueForKey:@"CommitteePhone"] != NULL){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[_carnivalObject valueForKey:@"CommitteePhone"]]];
    } else {
        UIAlertView *noPhone = [[UIAlertView alloc] initWithTitle:@"Error detected" message:@"Sorry. No phone number listed for this carnival committee." delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil];
        [noPhone show];
    }
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [controller dismissViewControllerAnimated:YES completion:NULL];
}


-(void)getExchangeRate{
    //NSArray* currencyISOCodes = [NSLocale commonISOCurrencyCodes];
    NSLocale *locale = [NSLocale currentLocale];
    //NSLog(@"Country %@| code %@", [_carnivalObject valueForKey:@"Country"], [_carnivalDairyData codefromCountry:[_carnivalObject valueForKey:@"Country"]]);
    
    //NSLog(@"Country code %@", [NSLocale localeIdentifierFromComponents:[NSDictionary dictionaryWithObject:@"KN" forKey:NSLocaleCountryCode]] );
    NSLocale *target = [[NSLocale alloc] initWithLocaleIdentifier:[NSLocale localeIdentifierFromComponents:[NSDictionary dictionaryWithObject:[_carnivalDairyData codefromCountry:[_carnivalObject valueForKey:@"Country"]] forKey:NSLocaleCountryCode]] ];
    
    
    
    _fromCurrency = [locale objectForKey:NSLocaleCurrencyCode];
    _toCurrency = @"INR";
    NSString * query = [[NSString stringWithFormat:@"https://query.yahooapis.com/v1/public/yql?env=store://datatables.org/alltableswithkeys&format=json&q=select * from yahoo.finance.xchange where pair in (\"%@%@\")", [locale objectForKey:NSLocaleCurrencyCode], [target objectForKey:NSLocaleCurrencyCode] ] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSURL *exchangeURL = [NSURL URLWithString:query];
    
    [NSURLConnection sendAsynchronousRequest:[NSURLRequest requestWithURL:exchangeURL] queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if (connectionError == nil && data != nil){
            _currencyDictionary = [[NSDictionary alloc] initWithDictionary:[[[[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil] valueForKey:@"query"] valueForKey:@"results"] valueForKey:@"rate"] ];
            //NSLog(@"Exchange date: %@", _currencyDictionary);
            // [[[[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil] valueForKey:@"query"] valueForKey:@"results"] valueForKey:@"rate"]);
        } else {
            NSLog(@"Error getting exchange rate data: %@", connectionError);
        }
    }];
    
    
    //NSLog(@"Currency code: %@", [target objectForKey:NSLocaleCurrencyCode]);
    //NSLog(@"Incoming: %@", [target displayNameForKey:NSLocaleCurrencyCode value:[target objectForKey:NSLocaleCurrencyCode]]);
    _fromCurrency = [locale displayNameForKey:NSLocaleCurrencyCode value:[locale objectForKey:NSLocaleCurrencyCode]];
    _fromCurrencySign = [locale objectForKey:NSLocaleCurrencySymbol];
    _toCurrency = [target displayNameForKey:NSLocaleCurrencyCode value:[target objectForKey:NSLocaleCurrencyCode]];
    _toCurrencySign = [target objectForKey:NSLocaleCurrencySymbol];
}

-(void)composeMessage{
    if ([_carnivalDairyData networkAvailable]){
    UIActionSheet *selectMessage = [[UIActionSheet alloc] initWithTitle:@"Select an action" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil
                                                      otherButtonTitles:@"Update Carnival Info", @"Add carnival venue", @"Share Carnival",@"Exchange Rates", @"Email Carnival Committee", @"Visit Carnival Website", nil];
    [selectMessage showInView:self.view];
    }
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    //NSLog(@"Button clicked: %li", (long)buttonIndex);
    if (buttonIndex == 0) {
        //[[NSNotificationCenter defaultCenter] postNotificationName:@"submitToJIRA" object:self userInfo:@{@"type":@"task"}];
        SubmitToJIRAViewController *submitToJIRA = [self.storyboard instantiateViewControllerWithIdentifier:@"submitToJIRA_vc"];
        submitToJIRA.issueType = @"task";
        [self.navigationController pushViewController:submitToJIRA animated:YES];
    } else if (buttonIndex == 1) {
        // Add a  point of interest
       if (![PFUser currentUser]){
            UIAlertView *loginAlert = [[UIAlertView alloc] initWithTitle:@"Login required" message:@"You need to be signed to submit a Caarnival Point of Interest" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Log me in", nil];
            loginAlert.tag = 1;
            [loginAlert show];
        } else {
            CarnivalPOIViewController *carnivalPOI = [self.storyboard instantiateViewControllerWithIdentifier:@"carnival_poi_vc"];
            carnivalPOI.carnivalId = [_carnivalObject valueForKey:@"objectId"];
            carnivalPOI.carnivalName = self.navigationItem.title;
            [self.navigationController pushViewController:carnivalPOI animated:YES];
        }
    } else if (buttonIndex == 2) {
        // Share carnival
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        NSLocale *enUSPOSIXLocale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
        [dateFormatter setLocale:enUSPOSIXLocale];
        [dateFormatter setDateFormat:@"EEEE MMM dd, yyyy"];
        
        NSString *carnivalToShare = [NSString stringWithFormat:@"Check it out! %@ starts on %@", self.navigationItem.title, [dateFormatter stringFromDate:[_carnivalObject valueForKey:@"Start"]]];
        //NSURL *appURL = [NSURL URLWithString:@"itms-apps://itunes.apple.com/app/xxxx"];
        NSString *appLink = [NSString stringWithFormat:@"Get the #carnivaltabancaapp on the App store."];
        UIActivityViewController *shareActivity = [[UIActivityViewController alloc] initWithActivityItems:@[carnivalToShare, appLink] applicationActivities:nil];
        
        NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                       UIActivityTypePrint,
                                       UIActivityTypeAssignToContact,
                                       UIActivityTypeSaveToCameraRoll,
                                       UIActivityTypeAddToReadingList,
                                       UIActivityTypePostToFlickr,
                                       UIActivityTypePostToVimeo, UIActivityTypeMail];
        shareActivity.excludedActivityTypes = excludeActivities;
        [self presentViewController:shareActivity animated:YES completion:nil];
    } else if (buttonIndex == 3){
        // Exchange Rate
        [self showExchangeRate];
    } else if (buttonIndex == 4) {
        // Email Carnival
        [self emailCommittee];
    } else if (buttonIndex == 5) {
        // Visit Website
        [self gotoWebsite];
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Show login view"
                                                            object:self
                                                          userInfo:nil];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
        [self styleNavigationBar];
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    [_locationManager requestWhenInUseAuthorization];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)hidesBottomBarWhenPushed{
    return YES;
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

//
//  NewCell.m
//  CarnivalTabanca
//
//  Created by Versatile Systems, Inc on 11/10/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import "VideosTableViewCell.h"

@implementation VideosTableViewCell

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


-(void)awakeFromNib{
    
    /*
     UIColor* mainColor = [UIColor colorWithRed:58.0/255 green:153.0/255 blue:216.0/255 alpha:1.0];
     UIColor* mainColorLight = [mainColor colorWithAlphaComponent:0.7];
     
     UIColor* neutralColor = [UIColor colorWithWhite:0.7 alpha:1.0];
     
     NSString* fontName = @"Avenir-Book";
     NSString* boldFontName = @"Avenir-Heavy";
     
     */
    //UIColor* mainColor = [UIColor colorWithRed:216.0/255 green:58.0/255 blue:58.0/255 alpha:1.0];
    //UIColor* mainColorLight = [mainColor colorWithAlphaComponent:0.85];
    
    UIColor* neutralColor = [UIColor colorWithWhite:0.25 alpha:1.0];
    
    NSString* fontName = @"EuphemiaUCAS";
    //NSString* boldFontName = @"EuphemiaUCAS-Bold";
    
    _voterCount.textColor = neutralColor;
    _voterCount.font =  [UIFont fontWithName:fontName size:12.0f];
    
    
    _playerView.layer.borderColor = [UIColor colorWithWhite:0.8f alpha:0.6f].CGColor;
    _playerView.layer.borderWidth = 1.0f;
    _playerView.layer.cornerRadius = 2.0f;
    _playerView.clipsToBounds = YES;
    _playerView.layer.cornerRadius = 7.0f;
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}


-(IBAction)thumbPressed:(UIButton *)sender{
    NSLog(@"button tag : %ld", (long)sender.tag);
    NSLog(@"Carnival ID: %@", _videolId);
    NSLog(@"Vote ID: %@", _voteId);
    NSLog(@"User: %@", [PFUser currentUser]);
    NSLog(@"_currentVote IN: %i", _currentVote);
    
    PFQuery *carnival = [PFQuery queryWithClassName:@"Videos"];
    PFQuery *votes = [PFQuery queryWithClassName:@"Votes"];
    int vote;
    if (sender.tag == 1){
        vote = -1;
    } else {
        vote = 1;
    }
    if (![PFUser currentUser]) {
        UIAlertView *loginAlert = [[UIAlertView alloc] initWithTitle:@"Login required" message:@"You need to be signed in make your vote count" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Log me in", nil];
        [loginAlert show];
    } else {
        // First check if vote already cast
        if (_currentVote == 0){
            NSLog(@"New Vote");
            [carnival getObjectInBackgroundWithId:_videolId block:^(PFObject *object, NSError *error) {
                if (error) {
                    NSLog(@"Error getting video object for new vote: %@", error);
                } else {
                    NSLog(@"Found video object, now add vote");
                    if (([[object valueForKey:@"votes"] intValue] < 1) && (vote == -1)){
                        [object incrementKey:@"voters"];
                    } else {
                        [object incrementKey:@"voters"];
                        [object incrementKey:@"votes" byAmount:[NSNumber numberWithInt:vote]];
                    }
                    [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                        if (error){
                            NSLog(@"Error recording new vote: %@", error);
                        } else {
                            NSLog(@"New vote recorded");
                            _currentVote = vote;
                            // Save vote object
                            PFObject *voteObject = [PFObject objectWithClassName:@"Votes"];
                            voteObject[@"assetId"] = _videolId;
                            voteObject[@"vote"] = [NSNumber numberWithInt:vote];
                            [voteObject saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                                if (error){
                                    NSLog(@"Error saving new vote: %@", error);
                                } else {
                                    NSLog(@"New vote successfully saved");
                                    //Now save vote relationship
                                    PFRelation *voteRelation = [[PFUser currentUser] relationForKey:@"votes"];
                                    [voteRelation addObject:voteObject];
                                    [[PFUser currentUser] saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                                        if (error){
                                            NSLog(@"Error saving vote relationship");
                                        } else {
                                            [_carnivalTabancaData getVotes];
                                            NSLog(@"Vote relationship saved");
                                        }
                                    }];
                                    
                                }
                            }];
                        }
                    }];
                }
            }];
        } else if ((_currentVote == -1) && (vote == 1)){
            NSLog(@"Switching -1 to +1");
            _currentVote = vote;
            [carnival getObjectInBackgroundWithId:_videolId block:^(PFObject *object, NSError *error) {
                if (error){
                    NSLog(@"Error switching -1 to +1: %@", error);
                } else {
                    [object incrementKey:@"votes" byAmount:[NSNumber numberWithInt:vote]];
                    [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                        if (error) {
                            NSLog(@"Error saving switch -1 to +1: %@", error);
                        } else {
                            [votes getObjectInBackgroundWithId:_voteId block:^(PFObject *object, NSError *error) {
                                if (error) {
                                    NSLog(@"Error getting vote to increment: %@", error);
                                } else {
                                    object[@"vote"] = [NSNumber numberWithInt:vote];
                                    [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                                        if (error){
                                            NSLog(@"Error saving incremented vote: %@", error);
                                        } else {
                                            [_carnivalTabancaData getVotes];
                                            NSLog(@"New incremented vote saved");
                                            NSLog(@"Switched from -1 to +1 successfully");
                                        }
                                    }];
                                }
                            }];
                            
                        }
                    }];
                }
            }];
        } else if ((_currentVote == 1) && (vote == -1)){
            NSLog(@"Switching +1 to -1");
            _currentVote = vote;
            [carnival getObjectInBackgroundWithId:_videolId block:^(PFObject *object, NSError *error) {
                if (error){
                    NSLog(@"Error switching +1 to -1: %@", error);
                } else {
                    [object incrementKey:@"votes" byAmount:[NSNumber numberWithInt:vote]];
                    [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                        if (error) {
                            NSLog(@"Error saving switch +1 to -1: %@", error);
                        } else {
                            [votes getObjectInBackgroundWithId:_voteId block:^(PFObject *object, NSError *error) {
                                if (error) {
                                    NSLog(@"Error getting vote to decrement: %@", error);
                                } else {
                                    object[@"vote"] = [NSNumber numberWithInt:vote];
                                    [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                                        if (error){
                                            NSLog(@"Error saving decremented vote: %@", error);
                                        } else {
                                            [_carnivalTabancaData getVotes];
                                            NSLog(@"New decremented vote saved");
                                            NSLog(@"Switched from +1 to -1 successfully");
                                        }
                                    }];
                                }
                            }];
                            
                            
                        }
                    }];
                }
                
            }];
        }
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Show login view"
                                                            object:self
                                                          userInfo:nil];
    }
}

-(void)showStars:(float)votes voters:(float)voters{
    //NSLog(@"Setting stars");
    _carnivalTabancaData = [CarnivalTabancaData sharedData];
    [_thumbsDown setBackgroundImage:[UIImage imageNamed:@"thumbs-down"] forState:UIControlStateNormal];
    [_thumbsUp setBackgroundImage:[UIImage imageNamed:@"thumbs-up"] forState:UIControlStateNormal];
    _starRating.delegate = self;
    _starRating.emptySelectedImage = [UIImage imageNamed:@"gray-StarEmpty"];
    _starRating.fullSelectedImage = [UIImage imageNamed:@"gray-StarFull"];
    _starRating.contentMode = UIViewContentModeScaleAspectFill;
    _starRating.maxRating = 5;
    _starRating.minRating = 1;
    if (voters > 0){
        _starRating.rating = (votes/voters)*_starRating.maxRating;
        if (_starRating.rating < 3) {
            _starRating.emptySelectedImage = [UIImage imageNamed:@"red-StarEmpty"];
            _starRating.fullSelectedImage = [UIImage imageNamed:@"red-StarFull"];
        } else {
            _starRating.emptySelectedImage = [UIImage imageNamed:@"StarEmpty"];
            _starRating.fullSelectedImage = [UIImage imageNamed:@"StarFull"];
        }
    } else {
        _starRating.rating = 0;
    }
    
    _starRating.editable = NO;
    _starRating.halfRatings = YES;
    _starRating.floatRatings = YES;
    _currentVote = 0;
    // NSLog(@"Looking for assetId: %@", _carnivalId);
    for (NSArray *vote in _carnivalTabancaData.votes){
        //NSLog(@"A vote: %@", _voteId = [vote valueForKey:@"objectId"]);
        if (([[vote valueForKey:@"assetId"] isEqualToString:_videolId]) && ([[vote valueForKey:@"vote"] intValue] == 1)){
            [_thumbsUp setBackgroundImage:[UIImage imageNamed:@"thumbs-up-selected"] forState:UIControlStateNormal];
            [_thumbsDown setBackgroundImage:[UIImage imageNamed:@"thumbs-down"] forState:UIControlStateNormal];
            _currentVote = 1;
            _voteId = [vote valueForKey:@"objectId"];
            //NSLog(@"Found a positive vote: %@", _voteId);
        } else if (([[vote valueForKey:@"assetId"] isEqualToString:_videolId]) && ([[vote valueForKey:@"vote"] intValue] == -1)) {
            [_thumbsDown setBackgroundImage:[UIImage imageNamed:@"thumbs-down-selected"] forState:UIControlStateNormal];
            [_thumbsUp setBackgroundImage:[UIImage imageNamed:@"thumbs-up"] forState:UIControlStateNormal];
            _currentVote = -1;
            _voteId = [vote valueForKey:@"objectId"];
            //NSLog(@"Found a negative vote: %@", _voteId);
        } else {
            
            //NSLog(@"No vote found");
        }
    }
    if ((int)voters == 1){
        [_voterCount setText:[NSString stringWithFormat:@"(%i vote)", (int)voters]];
    } else {
        [_voterCount setText:[NSString stringWithFormat:@"(%@ votes)", [_carnivalTabancaData suffixNumber:[NSNumber numberWithInt:(int)voters]]]];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(stopVideo)
                                                 name:@"stopVideoPlayBack"
                                               object:nil];
}

-(void)stopVideo{
    [_playerView stopVideo];
}

-(void)loadVideo:(NSString *)videoId{
    NSDictionary *playerVars = @{ @"playsinline" : @0, @"showinfo" : @1, @"modestbranding" : @1};
    [_playerView loadWithVideoId:videoId playerVars:playerVars];
}
@end

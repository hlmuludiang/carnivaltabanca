//
//  CarnivalDiaryData.m
//  myCarnivalDiary
//
//  Created by Versatile Systems, Inc on 10/27/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import "CarnivalTabancaData.h"

@implementation CarnivalTabancaData

static CarnivalTabancaData *sharedData = nil;

+(CarnivalTabancaData *) sharedData{
    if (sharedData == nil) {
        sharedData = [[CarnivalTabancaData alloc] init];
        [Parse setApplicationId:@"AUZCfbqQmGURqmXzgY6rsOzycGfFkeIYKtY1M44M"
                      clientKey:@"NWFmqCmylutmgLfBNhj9RG4EWDHwKhqj8pQC9ZDD"];
        [PFConfig getConfigInBackgroundWithBlock:^(PFConfig *config, NSError *error) {
            if (!error) {
                sharedData->_websiteURL = config[@"websiteURL"];
                sharedData->_privacyURL = config[@"privacyURL"];
                sharedData->_supportURL = config[@"supportURL"];
            }
        }];
        [PFFacebookUtils initializeFacebook];
        [PFTwitterUtils initializeWithConsumerKey:@"jBYsLvElkxREePM338qWUkMhh"
                                   consumerSecret:@"yhSMakUW5s1jUEcacvVoc44jedDh5PmIMt8czQ6udexoTBUlMh"];
        [Crashlytics startWithAPIKey:@"b5efc166c09f0bfcb91c141dbfdbbcd87c010956"];
        sharedData->_user = [PFUser user];
        sharedData->_currentUser = [PFUser currentUser];
        if (sharedData->_currentUser){
            sharedData->_isLoggedIn = YES;
        } else {
            sharedData->_isLoggedIn = NO;
        }
        sharedData->_sdk = [STAStartAppSDK sharedInstance];
        sharedData->_sdk.appID = @"210432535";
        sharedData->_sdk.devID = @"109374805";
        //[sharedData->_sdk showSplashAd];
        sharedData->_startAppAd = [[STAStartAppAd alloc] init];
        [sharedData->_startAppAd loadAd];
        sharedData->_startAppAdInterstitial = [[STAStartAppAd alloc] init];
        [sharedData->_startAppAdInterstitial loadAd:STAAdType_Automatic withDelegate:nil];
        [Flurry startSession:@"8TCFZJ48TTGJR79SPSVV"];
        
        sharedData->_internetConnection = [Reachability reachabilityForInternetConnection];
        //[[NSNotificationCenter defaultCenter] addObserver:sharedData selector:nil name:kReachabilityChangedNotification object:nil];
        [sharedData->_internetConnection startNotifier];
        
        
    }
    return sharedData;
}


-(void)showSplash{
    STASplashPreferences *splashPreferences = [[STASplashPreferences alloc] init];
    splashPreferences.splashMode = STASplashModeTemplate;
    splashPreferences.splashTemplateTheme = STASplashTemplateThemeOcean;
    splashPreferences.splashTemplateIconImageName = @"CarnivalTabanca.png";
    splashPreferences.splashTemplateAppName = @"Carnival Tabanca";
    
        [_sdk showSplashAdWithPreferences:splashPreferences];
    //[_sdk showSplashAd];
}


-(BOOL)networkAvailable{
   if ([_internetConnection isReachable]){
              //NSLog(@"Network up");
       return YES;

    } else {
        UIAlertView *networkAlert = [[UIAlertView alloc] initWithTitle:@"Network error" message:@"Please check your network connection and try again" delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil];
        [networkAlert show];
                //NSLog(@"Network Down");
        return NO;

    }
        
}

-(void)showInterstitialAd{
    [_startAppAdInterstitial showAd];
}

-(void)hideInterstitialAd{
}

-(void)showBannerAd{
    [_startAppAd showAd];
}

- (void) didDisplayBannerAd:(STABannerView*)banner{};
- (void) failedLoadBannerAd:(STABannerView*)banner withError:(NSError *)error{};
- (void) didClickBannerAd:(STABannerView*)banner{};

- (void) didLoadAd:(STAAbstractAd*)ad{};
- (void) failedLoadAd:(STAAbstractAd*)ad withError:(NSError *)error{};
- (void) didShowAd:(STAAbstractAd*)ad{};
- (void) failedShowAd:(STAAbstractAd*)ad withError:(NSError *)error{};
- (void) didCloseAd:(STAAbstractAd*)ad{};
- (void) didClickAd:(STAAbstractAd*)ad{};

-(NSString *)suffixNumber:(NSNumber *)number{
    if (!number)
        return @"";
    
    long long num = [number longLongValue];
    if (num < 1000)
        return [NSString stringWithFormat:@"%lld",num];
    
    int exp = (int) (log(num) / log(1000));
    NSArray * units = @[@"K",@"M",@"G",@"T",@"P",@"E"];
    
    int onlyShowDecimalPlaceForNumbersUnder = 1000; // Either 10, 100, or 1000 (i.e. 10 means 12.2K would change to 12K, 100 means 120.3K would change to 120K, 1000 means 120.3K stays as is)
    NSString *roundedNumStr = [NSString stringWithFormat:@"%.1f", (num / pow(1000, exp))];
    int roundedNum = (int)[roundedNumStr integerValue];
    if (roundedNum >= onlyShowDecimalPlaceForNumbersUnder) {
        roundedNumStr = [NSString stringWithFormat:@"%.0f", (num / pow(1000, exp))];
        roundedNum = (int)[roundedNumStr integerValue];
    }
    
    if (roundedNum >= 1000) { // This fixes a number like 999,999 from displaying as 1000K by changing it to 1.0M
        exp++;
        roundedNumStr = [NSString stringWithFormat:@"%.1f", (num / pow(1000, exp))];
    }
    
    NSString *result = [NSString stringWithFormat:@"%@%@", roundedNumStr, [units objectAtIndex:(exp-1)]];
    
    //NSLog(@"Original number: %@ - Result: %@", number, result);
    return result;
}

-(void)getVotes{
    //NSLog(@"getting likes for: %@", [PFUser currentUser]);
    if ([PFUser currentUser]){
        
        PFRelation *votes = [[PFUser currentUser] relationForKey:@"votes"];
        [[votes query] findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            if (error){
                NSLog(@"Error getting votes relationship: %@", error);
            } else {
                //NSLog(@"likes found: %@", objects);
                _votes = [[NSMutableArray alloc] initWithArray:objects];
                //NSLog(@"Got vote relationships: %@", _votes);
                //NSLog(@"Got votes: %@", _votes);
                [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadTable" object:self];
                
            }
        }];
    } else {
        _votes = nil;
    }
}

-(NSString *)codefromCountry:(NSString *)country{
    NSArray *countryCodes = [NSLocale ISOCountryCodes];
    NSMutableArray *countries = [NSMutableArray arrayWithCapacity:[countryCodes count]];
    for (NSString *countryCode in countryCodes)
    {
        NSString *identifier = [NSLocale localeIdentifierFromComponents: [NSDictionary dictionaryWithObject: countryCode forKey: NSLocaleCountryCode]];
        NSString *country = [[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"] displayNameForKey: NSLocaleIdentifier value: identifier];
        [countries addObject: country];
    }
    //
    NSDictionary *codeForCountryDictionary = [[NSDictionary alloc] initWithObjects:countryCodes forKeys:countries];
    //
    if ([codeForCountryDictionary valueForKey:country] == NULL){
        [self submitIssue:@"Unmatched countryname" content:[NSString stringWithFormat:@"Country missing: %@", country] type:@"bug"];
        NSLog(@"Country: %@", country);
        NSLog(@"Countries: %@" , countries);
        NSLog(@"ISO Codes: %@", codeForCountryDictionary);

    }
        return [codeForCountryDictionary valueForKey:country];
}
-(void)submitIssue:(NSString *)title content:(NSString *)content type:(NSString *)type{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://bitbucket.org/api/1.0/repositories/hlmuludiang/carnivaltabanca/issues/"]];
    request.HTTPMethod = @"POST";
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"Basic aGxtdWx1ZGlhbmc6JnBJWSFlcGQ0fA==" forHTTPHeaderField:@"Authorization"];
    NSString *stringData = [NSString stringWithFormat:@"title=%@&content=%@&status=new&kind=%@", title, content, type];
    NSData *requestBodyData = [stringData dataUsingEncoding:NSUTF8StringEncoding];
    request.HTTPBody = requestBodyData;
    [NSURLConnection  connectionWithRequest:request delegate:self];
    NSLog(@"Sent...");
}

-(UIImage *)button:(NSString *)color{
    return [[UIImage imageNamed:[NSString stringWithFormat:@"%@Button.png", color]] resizableImageWithCapInsets:UIEdgeInsetsMake(18, 18, 18, 18)];
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    _responseData = [[NSMutableData alloc] init];
    
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [_responseData appendData:data];
    
}

-(NSCachedURLResponse *)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse *)cachedResponse{
    return nil;
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection{
    // NSLog(@"Response: %@", [[NSString alloc] initWithData:_responseData encoding:NSUTF8StringEncoding]);
}


-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    NSLog(@"Error: %@", error);
}

-(void)login:(NSString *)emailAddress password:(NSString *)password{
    _user.username = emailAddress;
    _user.password = password;
    
    /*if (!_isLoggedIn){
     [_user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
     if (!error){
     _isLoggedIn = TRUE;
     
     }
     else {
     NSString *errorString = [error userInfo][@"error"];
     NSLog(@"Error message: %@", errorString);
     
     if ([errorString isEqualToString:@"username Email Address already taken"]){*/
    [PFUser logInWithUsernameInBackground:emailAddress password:password block:^(PFUser *user, NSError *error) {
        if (user){
            //login successfull
            _isLoggedIn = TRUE;
            [[NSNotificationCenter defaultCenter]
             postNotificationName:@"loginDone"
             object:self];
            NSLog(@"Good login");
        } else {
            NSString *errorString = [error userInfo][@"error"];
            NSLog(@"Error message: %@", errorString);
            _isLoggedIn = FALSE;
        }
    }];
    /* } else {
     
     _isLoggedIn = FALSE;
     UIAlertView *loginAlert = [[UIAlertView alloc] initWithTitle:@"Log in error detected" message:[NSString stringWithFormat:@"An error has been detected with your login attempt: %@", errorString] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
     [loginAlert show];}
     
     }
     }];
     }*/
}

-(BOOL)isUserLoggedIn{
    return _isLoggedIn;
}

@end

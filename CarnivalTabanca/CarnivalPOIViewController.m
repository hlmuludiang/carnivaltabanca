//
//  CarnivalPOIViewController.m
//  CarnivalTabanca
//
//  Created by Versatile Systems, Inc on 11/17/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import "CarnivalPOIViewController.h"

@interface CarnivalPOIViewController ()

@end

@implementation CarnivalPOIViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _carnivalDiaryData = [CarnivalTabancaData sharedData];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(submitPOI)];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(dismissView:)];
    _instructions.text = @"\tPlease at least the name of the location AND the type of location: \n\t Providing an address will allow us to quickly verify the location.";
    _POITypes = [[UIPickerView alloc] init];
    _POITypes.delegate = self;
    _POITypes.dataSource = self;
    _POITypes.showsSelectionIndicator = YES;
    
    _location.delegate = self;
    _locationAddress.delegate = self;
    _venueType.delegate = self;
    
    _pickerOptions = [[NSArray alloc] initWithObjects:@"Pick an location type", @"Carnival venue", @"Parade start", @"Parade end", @"Judging point", @"Mas camp", @"Public restrooms", nil];
    //_venueType.delegate = self;
    _venueType.inputView = _POITypes;
    _venueType.tag = 2;
    
    _locationManager = [[CLLocationManager alloc] init];
    _geoCoder = [[CLGeocoder alloc] init];
    
    [_getLocationButton setBackgroundImage:[_carnivalDiaryData button:@"green"] forState:UIControlStateNormal];
    self.navigationItem.prompt = _carnivalName;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if (textField.tag == 1){
        [self.view setFrame:CGRectMake(0, -90, self.view.frame.size.width, self.view.frame.size.height)];
    } else if (textField.tag == 2){
        [self.view setFrame:CGRectMake(0, -130, self.view.frame.size.width, self.view.frame.size.height)];
    } else if (textField.tag == 3 ){
        [self.view setFrame:CGRectMake(0, -150, self.view.frame.size.width, self.view.frame.size.height)];
    }
    return YES;
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    if (textField.tag == 1){
        [_locationAddress becomeFirstResponder];
    } else if (textField.tag == 2) {
        [_locationAddress resignFirstResponder];
        
    }
    return YES;
}

-(IBAction)dismissKeyboard:(id)sender{
    [_location resignFirstResponder];
    [_locationAddress resignFirstResponder];
    [_venueType resignFirstResponder];
    [self.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
}

- (IBAction)getCurrentLocation:(id)sender {
    _locationManager.delegate = self;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [_locationManager requestWhenInUseAuthorization];
    [_locationManager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        //
    }
    
    NSLog(@"Resolving the Address");
    [_geoCoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        NSLog(@"Found placemarks: %@, error: %@", placemarks, error);
        if (error == nil && [placemarks count] > 0) {
            _placemark = [placemarks lastObject];
            /*_locationAddress.text = [NSString stringWithFormat:@"%@ %@\n%@ %@\n%@\n%@",
                                 _placemark.subThoroughfare, _placemark.thoroughfare,
                                 _placemark.postalCode, _placemark.locality,
                                 _placemark.administrativeArea,
                                 _placemark.country];*/
            _locationAddress.text = [NSString stringWithFormat:@"%@ %@, %@, %@",
                                     _placemark.subThoroughfare, _placemark.thoroughfare,
                                    _placemark.locality, _placemark.administrativeArea
                                     ];
            [_locationManager stopUpdatingLocation];
        } else {
            NSLog(@"%@", error.debugDescription);
        }
    } ];

}

-(IBAction)dismissView:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)doneTouched:(id)sender{
    [_POITypes resignFirstResponder];
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return [_pickerOptions count];
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return _pickerOptions[row];
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    //NSLog(@"Type selected: %@", _pickerOptions[row]);
    _typePicked = (int)row;
    _venueType.text = _pickerOptions[row];
    [self.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [_venueType resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)submitPOI{
    if (_typePicked == 0 || [_location.text isEqualToString:@""]){
        UIAlertView *pickAlert = [[UIAlertView alloc] initWithTitle:@"Submission error detected." message:@"Please make sure you have provided a venue name AND selected a venue type." delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil];
        pickAlert.tag = 1;
        [pickAlert show];
    } else {
       
        
        PFObject *venue = [PFObject objectWithClassName:@"Venues"];
        venue[@"Location"] = _location.text;
        venue[@"Type"] = _pickerOptions[_typePicked];
        venue[@"Active"] = [NSNumber numberWithBool:NO];
        venue[@"Address"] = _locationAddress.text;
        
        [venue saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (error){
                NSLog(@"Error saving venue: %@", error);
            } else {
                PFObject *carnival = [PFObject objectWithoutDataWithClassName:@"Carnivals" objectId:_carnivalId];
                PFRelation *relationship = [carnival relationForKey:@"Venues"];
                [relationship addObject:venue];
                [carnival saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                    if (error){
                        NSLog(@"Error saving venue/carnival relatiosnhip: %@", error);
                    } else {
                        //Now save vote relationship
                        PFRelation *voteRelation = [[PFUser currentUser] relationForKey:@"Venues"];
                        [voteRelation addObject:venue];
                        [[PFUser currentUser] saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                            if (error){
                                NSLog(@"Error saving vote relationship");
                            } else {
                                NSLog(@"Vote relationship saved");
                                [_carnivalDiaryData submitIssue:[NSString stringWithFormat:@"Validate venueId: %@", [venue valueForKey:@"objectId"]] content:[NSString stringWithFormat:@"%@", venue] type:@"task"];
                                UIAlertView *locationSaved = [[UIAlertView alloc] initWithTitle:@"Location accepted" message:@"The location you submitted has been saved. It will be visible on the map once it has been verified." delegate:self cancelButtonTitle:@"Okay" otherButtonTitles: nil];
                                locationSaved.tag = 2;
                                [locationSaved show];
                            }
                        }];

                        

                    }
                }];
                
                }
        }];
        
        
        
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 2){
                            [self.navigationController popViewControllerAnimated:YES];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

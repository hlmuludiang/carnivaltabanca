//
//  AppDelegate.h
//  myCarnivalDiary
//
//  Created by Hilary Muludiang on 10/26/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CarnivalTabancaData.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate> {
    CarnivalTabancaData *_carnivalTabancaData;
}

@property (strong, nonatomic) UIWindow *window;



@end


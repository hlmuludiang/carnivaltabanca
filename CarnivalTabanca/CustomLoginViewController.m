//
//  CustomLoginViewController.m
//  CarnivalTabanca
//
//  Created by Versatile Systems, Inc on 11/9/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import "CustomLoginViewController.h"

@interface CustomLoginViewController ()

@end

@implementation CustomLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //[self.view setBackgroundColor:[UIColor clearColor]];
    //[self.logInView setBackgroundColor:[UIColor colorWithRed:58.0/255 green:153.0/255 blue:216.0/255 alpha:1]];
    UILabel *temp;
    temp.text = @"Carnival Tabanca";
    [temp setTextColor:[UIColor redColor]];
    [temp sizeToFit];
    [self.logInView setLogo:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Logo.png"]]];
    
   /* // Remove text shadow
    CALayer *layer = self.logInView.usernameField.layer;
    layer.shadowOpacity = 0;
    layer = self.logInView.passwordField.layer;
    layer.shadowOpacity = 0;
    
    // Set field text color
    [self.logInView.usernameField setBackgroundColor:[UIColor colorWithRed:58.0/255 green:153.0/255 blue:216.0/255 alpha:1]];
    [self.logInView.usernameField setPlaceholder:@"Username"];
    [self.logInView.usernameField setTextColor:[UIColor colorWithRed:216.0/255 green:58.0/255 blue:58.0/255 alpha:1.0]];
    [self.logInView.passwordField setBackgroundColor:[UIColor colorWithRed:58.0/255 green:153.0/255 blue:216.0/255 alpha:1]];
    [self.logInView.passwordField setPlaceholder:@"Password"];
    [self.logInView.passwordField setTextColor:[UIColor colorWithRed:216.0/255 green:58.0/255 blue:58.0/255 alpha:1.0]];*/
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  SecondViewController.m
//  myCarnivalDiary
//
//  Created by Hilary Muludiang on 10/26/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import "SettingsViewController.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController

-(BOOL)prefersStatusBarHidden{
    return NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    _carnivalTabancaData = [CarnivalTabancaData sharedData];
    //[_versionLabel setText:[NSString stringWithFormat:@"Version %@ (Build %@)", [[[NSBundle mainBundle] infoDictionary] valueForKey:@"CFBundleShortVersionString"], [[[NSBundle mainBundle] infoDictionary] valueForKey:@"CFBundleVersion"]]];
    _customButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 65, 25)];
    
    
    [_customButton setBackgroundImage:[_carnivalTabancaData button:@"green"] forState:UIControlStateNormal];
    [_customButton setTitle:@"Login" forState:UIControlStateNormal];
    [_customButton addTarget:self action:@selector(loginLogout:) forControlEvents:UIControlEventTouchUpInside];
    _llButton = [[UIBarButtonItem alloc] initWithCustomView:_customButton];
    
    
    self.navigationItem.rightBarButtonItem = _llButton;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"help"] style:UIBarButtonItemStyleDone target:self action:@selector(helpDocs:)];
    self.navigationItem.leftBarButtonItem.tintColor = [UIColor blackColor];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userLoggedIn) name:@"loginCompleted" object:nil];
    
    _tableView.delegate = self;
    NSMutableDictionary *fbDictionary = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"FB.png", @"image", @"FaceBook Connect", @"title", @"socialCell", @"cellType", nil];
    NSMutableDictionary *twitterDictionary = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"Twitter.png", @"image", @"Twitter Connect", @"title", @"socialCell", @"cellType", nil];
   /*NSDictionary *websiteDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:@"Carnival Tabanca website", @"title", @"contactCell", @"cellType", nil];
    NSDictionary *supportDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:@"Support Website", @"title", @"contactCell", @"cellType", nil];
    NSDictionary *privacyDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:@"Privacy Notice", @"title", @"contactCell", @"cellType", nil];*/
    NSDictionary *contactDevDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:@"Contact Developer", @"title", @"contactCell", @"cellType", nil];
    NSDictionary *featureRequestDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:@"Request a Feature", @"title", @"contactCell", @"cellType", nil];
    NSDictionary *versionDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:@"Version", @"title", @"dataCell", @"cellType", nil];
    _tableData = [[NSArray alloc] initWithObjects:fbDictionary, twitterDictionary, contactDevDictionary, featureRequestDictionary, versionDictionary, nil];
    
}

-(void)getFacebookData{
    //NSLog(@"Getting facebook profile pic");
    if ([PFFacebookUtils isLinkedWithUser:[PFUser currentUser]]) {
    FBRequest *request = [FBRequest requestForMe];
    [request startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        if (!error){
            NSDictionary *userData = (NSDictionary *)result;
            NSString *faceBookId = userData[@"id"];
            NSURL *pictureURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large&return_ssl_resources=1", faceBookId]];
            NSURLRequest *urlRequest = [NSURLRequest requestWithURL:pictureURL];
            [NSURLConnection sendAsynchronousRequest:urlRequest queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                if (connectionError == nil && data != nil){
                   // _facebookProfileImage.image = [UIImage imageWithData:data];
                    //[_linkToFaceBook.titleLabel setText:@"Unlink from Facebook"];
                    [_tableData[0] setValue:@"Facebook Disconnect" forKey:@"title"];
                    [_tableData[0] setValue:[UIImage imageWithData:data] forKey:@"image"];
                    [_tableView reloadData];
                } else {
                    NSLog(@"Error getting facebook profil pic: %@", connectionError);
                }
            }];
        } else {
            NSLog(@"Error getting Facebook data: %@", error);
        }
    }];
}
}

-(void)getTwitterData{
    if ([PFTwitterUtils isLinkedWithUser:[PFUser currentUser]]){
    NSURL *verify = [NSURL URLWithString:@"https://api.twitter.com/1.1/account/verify_credentials.json"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:verify];
    [[PFTwitterUtils twitter] signRequest:request];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if (connectionError == nil && data != nil) {
            NSArray *x = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            //_twitterProfileImage.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[x valueForKey:@"profile_image_url_https"]]]];
            //[_linkToTwitter setTitle:@"Unlink from Twitter" forState:UIControlStateNormal];
            [_tableData[1] setValue:@"Twitter Disconnect" forKey:@"title"];
            [_tableData[1] setValue:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[x valueForKey:@"profile_image_url_https"]]]] forKey:@"image"];
            [_tableView reloadData];

        } else {
            NSLog(@"Error getting twitter data: %@", connectionError);
        }
    }];
    }
}

-(IBAction)helpDocs:(UIBarButtonItem *)sender{
    if ([_carnivalTabancaData networkAvailable]){
    UIActionSheet *helpMessage = [[UIActionSheet alloc] initWithTitle:@"Select a link" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil
                                                      otherButtonTitles:@"Visit Carnival Tabanca Website", @"Support", @"Privacy Notice", nil];
    [helpMessage showInView:self.view];
    }
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0) {
        if (![[_carnivalTabancaData websiteURL] isEqualToString:@""]){
         CarnivalWebsiteViewController *carnivalWebsite = [self.storyboard instantiateViewControllerWithIdentifier:@"Carnival_website_vc"];
         carnivalWebsite.websiteURL = [_carnivalTabancaData websiteURL];
         [self.navigationController pushViewController:carnivalWebsite animated:YES];
         } else {
         UIAlertView *noWebsite = [[UIAlertView alloc] initWithTitle:@"Error detected" message:@"Sorry. No website listed for this carnival." delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil];
         [noWebsite show];
         }
    } else if (buttonIndex == 1){
        if (![[_carnivalTabancaData supportURL] isEqualToString:@""]){
         CarnivalWebsiteViewController *carnivalWebsite = [self.storyboard instantiateViewControllerWithIdentifier:@"Carnival_website_vc"];
         carnivalWebsite.websiteURL = [_carnivalTabancaData supportURL];
         [self.navigationController pushViewController:carnivalWebsite animated:YES];
         } else {
         UIAlertView *noWebsite = [[UIAlertView alloc] initWithTitle:@"Error detected" message:@"Sorry. No website listed for this carnival." delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil];
         [noWebsite show];
         }
    } else if (buttonIndex == 2){
         if (![[_carnivalTabancaData privacyURL] isEqualToString:@""]){
         CarnivalWebsiteViewController *carnivalWebsite = [self.storyboard instantiateViewControllerWithIdentifier:@"Carnival_website_vc"];
         carnivalWebsite.websiteURL = [_carnivalTabancaData privacyURL];
         [self.navigationController pushViewController:carnivalWebsite animated:YES];
         } else {
         UIAlertView *noWebsite = [[UIAlertView alloc] initWithTitle:@"Error detected" message:@"Sorry. No website listed for this carnival." delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil];
         [noWebsite show];
         }
    }
}

-(IBAction)contactEndpoint:(UIButton *)sender{
    NSLog(@"Contact button %i", (int)sender.tag);
    if (sender.tag == 2) {
        NSArray *emailRecipient = [NSArray arrayWithObject:@"carnivaltabancaapp@gmail.com"];
        MFMailComposeViewController *composeEmail = [[MFMailComposeViewController alloc] init];
        composeEmail.mailComposeDelegate = self;
        [composeEmail setSubject:@"Email from Carnival Tabanca App"];
        [composeEmail setToRecipients:emailRecipient];
        [self presentViewController:composeEmail animated:YES completion:NULL];
    } else if (sender.tag == 3){
        SubmitToJIRAViewController *submitToJIRA = [self.storyboard instantiateViewControllerWithIdentifier:@"submitToJIRA_vc"];
        submitToJIRA.issueType = @"proposal";
        [self.navigationController pushViewController:submitToJIRA animated:YES];
    }
    
}


- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [controller dismissViewControllerAnimated:YES completion:NULL];
}

-(IBAction)linkUnlink:(UIButton *)sender{
    if (sender.tag == 0){
        if (![PFUser currentUser]){
            [PFFacebookUtils logInWithPermissions:@[] block:^(PFUser *user, NSError *error) {
                if (user){
                    
                    [self userLoggedIn];
                    [self getFacebookData];
                    [self getTwitterData];
                }
            }];
        } else if (![PFFacebookUtils isLinkedWithUser:[PFUser currentUser]]) {
            [PFFacebookUtils linkUser:[PFUser currentUser] permissions:@[@"email",@"user_friends",@"public_profile"] block:^(BOOL succeeded, NSError *error) {
                if (succeeded && !error){
                    
                    [self getFacebookData];
                    [self getTwitterData];
                } else {
                    NSLog(@"Error linking to facebook: %@", error);
                }
            }];
        } else {
            [PFFacebookUtils unlinkUserInBackground:[PFUser currentUser] block:^(BOOL succeeded, NSError *error) {
                if (succeeded && !error){
                    [_tableData[0] setValue:@"Facebook Disconnect" forKey:@"title"];
                    [_tableView reloadData];
                } else {
                    NSLog(@"Error unlinking user from facebook: %@", error);
                }
            }];
        }
    } else {
        //Twitter login/out
        if (![PFUser currentUser]) {
            [PFTwitterUtils logInWithBlock:^(PFUser *user, NSError *error) {
                if (user){
                    [self userLoggedIn];
                    [self getTwitterData];
                    [self getFacebookData];
                }
            }];
        } else if (![PFTwitterUtils isLinkedWithUser:[PFUser currentUser]]){
            [PFTwitterUtils linkUser:[PFUser currentUser] block:^(BOOL succeeded, NSError *error) {
                if (succeeded && !error){
                    [self getTwitterData];
                } else {
                    NSLog(@"Error linking to twitter: %@", error);
                }
            }];
        } else {
            [PFTwitterUtils unlinkUserInBackground:[PFUser currentUser] block:^(BOOL succeeded, NSError *error) {
                if (!error && succeeded){
                    [_tableData[1] setValue:@"Twitter Disconnect" forKey:@"title"];
                    [_tableView reloadData];
                } else {
                    NSLog(@"Error unlinking from Twitter: %@", error);
                }
            }];
        }
    }
}

-(void)userLoggedIn{
    [_customButton setBackgroundImage:[_carnivalTabancaData button:@"orange"] forState:UIControlStateNormal];
    [_customButton setTitle:@"Logout" forState:UIControlStateNormal];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self styleNavigationBar];
        [_carnivalTabancaData showBannerAd];
    if ([PFUser currentUser]) {
        [_customButton setBackgroundImage:[_carnivalTabancaData button:@"orange"] forState:UIControlStateNormal];
        [_customButton setTitle:@"Logout" forState:UIControlStateNormal];
    }
    /*if (_carnivalTabancaData.isUserLoggedIn){
        [[self.tabBarController.tabBar.items objectAtIndex:2] setFinishedSelectedImage:[UIImage imageNamed:@"user.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"user.png"]];
    } else {
        [[self.tabBarController.tabBar.items objectAtIndex:2] setFinishedSelectedImage:[UIImage imageNamed:@"anonymous.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"anonymous.png"]];
    }*/
    
    if (![PFFacebookUtils isLinkedWithUser:[PFUser currentUser]]) {
        //[_tableData[0] setValue:@"Link to Facebook" forKey:@"title"];
        //[_tableView reloadData];

    } else {
        //[_tableData[0] setValue:@"Unlink from Facebook" forKey:@"title"];

        [self getFacebookData];
    }
    
    if (![PFTwitterUtils isLinkedWithUser:[PFUser currentUser]]) {
        //[_tableData[0] setValue:@"Link to Facebook" forKey:@"title"];

    } else {
        //[_tableData[0] setValue:@"Unlink from Facebook" forKey:@"title"];
        [self getTwitterData];
    }
        [self setTitle:@"Profile & Help"];
}

-(IBAction)loginLogout:(id)sender{
    if (![PFUser currentUser]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Show login view"
                                                            object:self
                                                          userInfo:nil];
    } else {
        [PFUser logOut];
        [_customButton setBackgroundImage:[_carnivalTabancaData button:@"green"] forState:UIControlStateNormal];
        [_customButton setTitle:@"Login" forState:UIControlStateNormal];
        [_carnivalTabancaData getVotes];
        //[_facebookProfileImage setImage:[UIImage imageNamed:@"FB.png"]];
        //[_twitterProfileImage setImage:[UIImage imageNamed:@"Twitter.png"]];
        NSLog(@"Logged out");
        //[_linkToFaceBook setTitle:@"Link to Facebook" forState:UIControlStateNormal];
        //[_linkToTwitter setTitle:@"Link To Twitter" forState:UIControlStateNormal];
        [_tableData[0] setValue:@"FB.png" forKey:@"image"];
        [_tableData[0] setValue:@"Facebook Connect" forKey:@"title"];
        [_tableData[1] setValue:@"Twitter.png" forKey:@"image"];
        [_tableData[1] setValue:@"Twitter Connect" forKey:@"title"];
        [_tableView reloadData];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadTable"
                                                            object:self
                                                    userInfo:nil];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)styleNavigationBar{
    
    NSString* navigationTitleFont = @"Avenir-Heavy";
    UIColor* color = [UIColor colorWithRed:58.0/255 green:153.0/255 blue:216.0/255 alpha:1.0];
    
   //NSString* navigationTitleFont = @"EuphemiaUCAS-Bold";
    //UIColor* color = [UIColor colorWithRed:216.0/255 green:58.0/255 blue:58.0/255 alpha:1.0];
    
    self.navigationController.navigationBar.barTintColor = color;
    
    self.navigationController.navigationBar.titleTextAttributes =
    
    @{NSForegroundColorAttributeName : [UIColor whiteColor],
      NSFontAttributeName : [UIFont fontWithName:navigationTitleFont size:18.0f]
      };
    
    
    /*UIImageView* searchView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"search.png"]];
     searchView.frame = CGRectMake(0, 0, 20, 20);
     
     UIBarButtonItem* searchItem = [[UIBarButtonItem alloc] initWithCustomView:searchView];
     
     self.navigationItem.rightBarButtonItem = searchItem;
     
     UIButton* menuButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 28, 20)];
     [menuButton setImage:[UIImage imageNamed:@"menu.png"] forState:UIControlStateNormal];
     [menuButton addTarget:self action:@selector(dismissView:) forControlEvents:UIControlEventTouchUpInside];
     
     UIBarButtonItem* menuItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
     self.navigationItem.leftBarButtonItem = menuItem;*/
}

#pragma mark - UITableViewDataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_tableData count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString * cellIdentifier;
    id cell;
    
    if ([[[_tableData objectAtIndex:indexPath.row] valueForKey:@"cellType"] isEqualToString:@"socialCell"]){
        cellIdentifier = @"socialCell";
        cell = (SocialTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil) {
            cell = [[SocialTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];}
        if ([[[_tableData objectAtIndex:indexPath.row] valueForKey:@"image"] isKindOfClass:[NSString class]]) {
            [cell setProfile:[[_tableData objectAtIndex:indexPath.row] valueForKey:@"title"] image:[UIImage imageNamed:[[_tableData objectAtIndex:indexPath.row] valueForKey:@"image"]] buttonTag:indexPath.row];
        } else {
            [cell setProfile:[[_tableData objectAtIndex:indexPath.row] valueForKey:@"title"] image:[[_tableData objectAtIndex:indexPath.row] valueForKey:@"image"] buttonTag:indexPath.row];
        }
        
            
    } else if ([[[_tableData objectAtIndex:indexPath.row] valueForKey:@"cellType"] isEqualToString:@"contactCell"]){
        cellIdentifier = @"contactCell";
        cell = (ContactTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil) {
            cell = [[ContactTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
        [cell setContact:[[_tableData objectAtIndex:indexPath.row] valueForKey:@"title"] buttonTag:indexPath.row];
    } else {
        cellIdentifier = @"dataCell";
        cell = (DataTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil) {
            cell = [[DataTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
        if ([[[_tableData objectAtIndex:indexPath.row] valueForKey:@"title"] isEqualToString:@"Version"]) {
            [cell setDataLabel:[NSString stringWithFormat:@"Version %@ (Build %@)", [[[NSBundle mainBundle] infoDictionary] valueForKey:@"CFBundleShortVersionString"], [[[NSBundle mainBundle] infoDictionary] valueForKey:@"CFBundleVersion"]]];
        } else {
        [cell setData:[[_tableData objectAtIndex:indexPath.row] valueForKey:@"title"]];
        }
    }
    
    
    
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44.0f;
}
#pragma mark - UITableViewDelegate


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

@end

//
//  AuthenticateUserViewController.h
//  myCarnivalDiary
//
//  Created by Versatile Systems, Inc on 10/27/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CarnivalTabancaData.h"

@interface AuthenticateUserViewController : UIViewController {
    CarnivalTabancaData *_carnivalTabancaData;
    UIBarButtonItem *_cancelButton;
    IBOutlet UITextField *_emailAddress;
    IBOutlet UITextField *_password;
    IBOutlet UIButton *_submitButton;
    UIActivityIndicatorView *_activityView;
    IBOutlet UIButton *_dismissKeyboard;
}

@end

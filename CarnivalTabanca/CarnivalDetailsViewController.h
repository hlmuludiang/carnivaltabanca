//
//  CarnivalDetailsViewController.h
//  myCarnivalDiary
//
//  Created by Hilary Muludiang on 10/26/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <Parse/Parse.h>
#import "CarnivalTabancaData.h"
#import "CarnivalWebsiteViewController.h"
#import <MessageUI/MessageUI.h>
#import "SubmitToJIRAViewController.h"
#import "CarnivalPOIViewController.h"
#import "ZSPinAnnotation.h"
#import "ZSAnnotation.h"

@interface CarnivalDetailsViewController : UIViewController <UIAlertViewDelegate, UIActionSheetDelegate, CLLocationManagerDelegate, MFMailComposeViewControllerDelegate, MKMapViewDelegate> {

    NSString *_carnivalDates;

    IBOutlet MKMapView *_mapView;
    CLLocationManager *_locationManager;
    NSString *_fromCurrency;
    NSString *_toCurrency;
    NSString *_fromCurrencySign;
    NSString *_toCurrencySign;
    NSDictionary *_currencyDictionary;
    CarnivalTabancaData *_carnivalDairyData;
    PFRelation *_carnivalLocations;
    NSArray *_venues;
    NSMutableArray *_annotationsArray;
}

@property (weak, nonatomic) PFObject *carnivalObject;

@end

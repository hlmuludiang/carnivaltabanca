//
//  AuthenticateUserViewController.m
//  myCarnivalDiary
//
//  Created by Versatile Systems, Inc on 10/27/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import "AuthenticateUserViewController.h"

@interface AuthenticateUserViewController ()

@end

@implementation AuthenticateUserViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _carnivalTabancaData = [CarnivalTabancaData sharedData];
    
    
    _cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelLogin:)];
    _cancelButton.tintColor = [UIColor redColor];
    self.navigationItem.rightBarButtonItem = _cancelButton;
}


-(IBAction)submitCredentials:(id)sender{
    
    if (([self validEmail:_emailAddress.text]) && ([self validPassword:_password.text])){
        _activityView=[[UIActivityIndicatorView alloc]     initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        
        _activityView.center=self.view.center;
        
        [_activityView startAnimating];
        
        [self.view addSubview:_activityView];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(testNotif) name:@"loginDone" object:nil];
        
        [_carnivalTabancaData login:_emailAddress.text password:_password.text];
        if ([_carnivalTabancaData isUserLoggedIn]){
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }
}

-(IBAction)dismissKeyboard:(id)sender{
    NSLog(@"Dismissing keyboard...");
    [_emailAddress resignFirstResponder];
    [_password resignFirstResponder];
}

-(BOOL)validEmail:(NSString *)emailString{
    if ([emailString length] == 0){
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Email address missing" message:@"Please provide an email address" delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil];
        [alertView show];
        return NO;
    }
    
    NSString *regExPattern = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    
    NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regExPattern options:NSRegularExpressionCaseInsensitive error:nil];
    NSUInteger regExMatches = [regEx numberOfMatchesInString:emailString options:0 range:NSMakeRange(0, [emailString length])];
    
    //NSLog(@"%i", regExMatches);
    if (regExMatches == 0) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Email address invalid" message:@"Please re-check the email address you have provided." delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil];
        [alertView show];
        return NO;
    } else {
        return YES;
    }
}

-(BOOL)validPassword:(NSString *)passwordString{
    if ([passwordString length] < 6){
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Password too short" message:@"Please provide a password at least six (6) characters long." delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil];
        [alertView show];
        return NO;
    } else {
        return YES;
    }
}

-(void)testNotif{
    [_activityView stopAnimating];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)cancelLogin:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

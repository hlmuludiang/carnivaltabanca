//
//  CarnivalDiaryData.h
//  myCarnivalDiary
//
//  Created by Versatile Systems, Inc on 10/27/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>
#import <StartApp/StartApp.h>
#import <Crashlytics/Crashlytics.h>
#import <FacebookSDK/FacebookSDK.h>
#import <ParseFacebookUtils/PFFacebookUtils.h>
#import "Flurry.h"
#import "Reachability.h"

@interface CarnivalTabancaData : NSObject <NSURLConnectionDataDelegate, STADelegateProtocol> {
   BOOL _isLoggedIn;
    PFUser *_user;
    PFUser *_currentUser;
    NSMutableData *_responseData;
    STAStartAppSDK *_sdk;
    STAStartAppAd *_startAppAd;
    STAStartAppAd *_startAppAdInterstitial;
    BOOL _isNetworkUp;
    Reachability *_internetConnection;
}

@property (nonatomic) NSMutableArray *votes;
@property (nonatomic) NSString *websiteURL;
@property (nonatomic) NSString *supportURL;
@property (nonatomic) NSString *privacyURL;

+(CarnivalTabancaData *) sharedData;


-(BOOL)isUserLoggedIn;
-(void)login:(NSString *)emailAddress password:(NSString *)password;
-(void)submitIssue:(NSString *)title content:(NSString *)content type:(NSString *)type;
-(NSString *)codefromCountry:(NSString *)country;
-(void)getVotes;
-(NSString *)suffixNumber:(NSNumber *)number;
//-(void)newVoteRelationship:(NSString *)userId vote:(float)vote;
-(void)showInterstitialAd;
-(void)hideInterstitialAd;
-(void)showBannerAd;
-(UIImage *)button:(NSString *)color;
-(BOOL)networkAvailable;
-(void)showSplash;
@end

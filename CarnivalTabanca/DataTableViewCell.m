//
//  DataTableViewCell.m
//  CarnivalTabanca
//
//  Created by Versatile Systems, Inc on 11/23/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import "DataTableViewCell.h"

@implementation DataTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setDataLabel:(NSString *)title{
    [_dataLabel setText:title];
}

@end

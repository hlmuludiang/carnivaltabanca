//
//  CarnivalWebsiteViewController.h
//  CarnivalTabanca
//
//  Created by Versatile Systems, Inc on 11/15/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CarnivalWebsiteViewController : UIViewController{
IBOutlet UIWebView *_carnivalWebView;
}

@property (nonatomic, weak) NSString *websiteURL;
@end

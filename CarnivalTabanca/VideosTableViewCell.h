//
//  NewCell.h
//  CarnivalTabanca
//
//  Created by Versatile Systems, Inc on 11/10/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import <Parse/Parse.h>
#import "CarnivalTabancaData.h"
#import "TPFloatRatingView.h"
#import "YTPlayerView.h"

@interface VideosTableViewCell : PFTableViewCell <TPFloatRatingViewDelegate> {
    CarnivalTabancaData *_carnivalTabancaData;
    IBOutlet YTPlayerView *_playerView;
    IBOutlet TPFloatRatingView *_starRating;
    IBOutlet UIButton *_thumbsUp;
    IBOutlet UIButton *_thumbsDown;
    int _currentVote;
    NSString *_voteId;
    IBOutlet UILabel *_voterCount;
}


@property (weak, nonatomic) NSString *videolId;
-(void)loadVideo:(NSString *)videoId;
-(void)showStars:(float)votes voters:(float)voters;

@end

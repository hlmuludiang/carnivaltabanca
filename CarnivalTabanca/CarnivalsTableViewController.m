//
//  NewCarnivalsTableViewController.m
//  CarnivalTabanca
//
//  Created by Versatile Systems, Inc on 11/9/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import "CarnivalsTableViewController.h"

@implementation CarnivalsTableViewController

-(BOOL)prefersStatusBarHidden{
    return NO;
}

- (id)initWithCoder:(NSCoder *)aCoder {
    if (self = [super initWithCoder:aCoder]) {
        // Custom the table
        
        // The className to query on
        self.parseClassName = @"Carnivals";
        
        // The key of the PFObject to display in the label of the default cell style
        self.textKey = @"Country";
        
        // Uncomment the following line to specify the key of a PFFile on the PFObject to display in the imageView of the default cell style
        // self.imageKey = @"image";
        
        // Whether the built-in pull-to-refresh is enabled
        self.pullToRefreshEnabled = YES;
        
        // Whether the built-in pagination is enabled
        self.paginationEnabled = NO;
        
        // The number of objects to show per page
        self.objectsPerPage = 0;
    }
    return self;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

-(void)showLoginView{
    _logInController = [[CustomLoginViewController alloc] init];
    _logInController.delegate = self;
    _logInController.fields = PFLogInFieldsDefault
    | PFLogInFieldsLogInButton
    | PFLogInFieldsSignUpButton
    | PFLogInFieldsPasswordForgotten
    | PFLogInFieldsDismissButton
    | PFLogInFieldsFacebook
    | PFLogInFieldsTwitter;
    _signUpController = [[CustomSignUpViewController alloc] init];
    _signUpController.delegate = self;
    _logInController.signUpController = _signUpController;
    _logInController.signUpController.delegate = self;
    [_logInController.logInView.signUpButton removeTarget:nil action:nil forControlEvents:UIControlEventAllEvents];
    [_logInController.logInView.signUpButton addTarget:self action:@selector(signUpButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self presentViewController:_logInController animated:YES completion:nil];
}

-(void)logInViewController:(PFLogInViewController *)logInController didLogInUser:(PFUser *)user{
    [self dismissViewControllerAnimated:YES completion:^{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"loggedIn"
                                                            object:self
                                                          userInfo:nil];
        [_carnivalTabancaData getVotes];
        [self loadObjects];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"loginCompleted"
                                                            object:self
                                                          userInfo:nil];
    }];
    
}

-(IBAction)signUpButtonClicked:(id)sender{
    //NSLog(@"sign up");
    [_logInController dismissViewControllerAnimated:YES completion:nil];
    [self presentViewController:_logInController.signUpController animated:YES completion:nil];
}

- (BOOL)logInViewController:(PFLogInViewController *)logInController shouldBeginLogInWithUsername:(NSString *)username password:(NSString *)password {
    if (username && password && username.length && password.length) {
        return YES;
    }
    
    [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Missing Information", nil) message:NSLocalizedString(@"Make sure you fill out all of the information!", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil] show];
    return NO;
}

-(void)logInViewController:(CustomLoginViewController *)logInController didFailToLogInWithError:(NSError *)error{
    //[self dismissViewControllerAnimated:YES completion:nil];
    UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Error logging in" message:@"Please verify that you have enter the correct username and password" delegate:self cancelButtonTitle:@"Okay" otherButtonTitles: nil];
    [errorAlert show];
}

-(void)logInViewControllerDidCancelLogIn:(CustomLoginViewController *)logInController{
    //[self dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)signUpViewController:(PFSignUpViewController *)signUpController shouldBeginSignUp:(NSDictionary *)info {
    BOOL informationComplete = YES;
    for (id key in info) {
        NSString *field = [info objectForKey:key];
        if (!field || field.length == 0) {
            informationComplete = NO;
            break;
        }
    }
    
    if (!informationComplete) {
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Missing Information", nil) message:NSLocalizedString(@"Make sure you fill out all of the information!", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil] show];
    }
    
    return informationComplete;
}

-(void)signUpViewController:(CustomSignUpViewController *)signUpController didSignUpUser:(PFUser *)user{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)signUpViewController:(PFSignUpViewController *)signUpController didFailToSignUpWithError:(NSError *)error {
    NSLog(@"Failed to sign up...");
}

-(void)signUpViewControllerDidCancelSignUp:(CustomSignUpViewController *)signUpController{
    
}
-(void)submitToJira:(NSNotification *)notification{
    //NSLog(@"Notification: %@", [[notification userInfo] valueForKey:@"type"]);
    SubmitToJIRAViewController *submitToJIRA = [self.storyboard instantiateViewControllerWithIdentifier:@"submitToJIRA_vc"];
    submitToJIRA.issueType = [[notification userInfo] valueForKey:@"type"];
    [self.navigationController pushViewController:submitToJIRA animated:YES];
    //[self.view.window.rootViewController presentViewController:submitToJIRA animated:YES completion:nil];
}


-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 0){
        if (buttonIndex == 1){
            //NSLog(@"User: %@ | Password: %@", [alertView textFieldAtIndex:0].text, [alertView textFieldAtIndex:1].text);
        }
    }
}


#pragma mark - UIViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    _carnivalTabancaData = [CarnivalTabancaData sharedData];
    //[_carnivalTabancaData getVotes];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadData) name:@"reloadTable" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showLoginView)
                                                 name:@"Show login view"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(submitToJira:)
                                                 name:@"submitToJIRA"
                                               object:nil];

}

-(void)styleNavigationBar{
    
    NSString* navigationTitleFont = @"Avenir-Heavy";
    UIColor* color = [UIColor colorWithRed:58.0/255 green:153.0/255 blue:216.0/255 alpha:1.0];
    
    //NSString* navigationTitleFont = @"EuphemiaUCAS-Bold";
    //UIColor* color = [UIColor colorWithRed:216.0/255 green:58.0/255 blue:58.0/255 alpha:1.0];
    
    self.navigationController.navigationBar.barTintColor = color;
    
    self.navigationController.navigationBar.titleTextAttributes =
    
    @{NSForegroundColorAttributeName : [UIColor whiteColor],
      NSFontAttributeName : [UIFont fontWithName:navigationTitleFont size:18.0f]
      };
    
    
    /*UIImageView* searchView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"search.png"]];
     searchView.frame = CGRectMake(0, 0, 20, 20);
     
     UIBarButtonItem* searchItem = [[UIBarButtonItem alloc] initWithCustomView:searchView];
     
     self.navigationItem.rightBarButtonItem = searchItem;
     
     UIButton* menuButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 28, 20)];
     [menuButton setImage:[UIImage imageNamed:@"menu.png"] forState:UIControlStateNormal];
     [menuButton addTarget:self action:@selector(dismissView:) forControlEvents:UIControlEventTouchUpInside];
     
     UIBarButtonItem* menuItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
     self.navigationItem.leftBarButtonItem = menuItem;*/
}


-(void)reloadData{
    [self loadObjects];
    [self.tableView reloadData];
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self styleNavigationBar];
    [self.view layoutIfNeeded];
    [_carnivalTabancaData showBannerAd];
        //[_carnivalTabancaData showInterstitialAd];
    [_carnivalTabancaData getVotes];
    [self loadObjects];

}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    //[self.tableView reloadData];
        [self setTitle:@"Carnivals"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


#pragma mark - PFQueryTableViewController

- (void)objectsWillLoad {
    [super objectsWillLoad];
    
    // This method is called before a PFQuery is fired to get more objects
}

- (void)objectsDidLoad:(NSError *)error {
    [super objectsDidLoad:error];
    
    // This method is called every time objects are loaded from Parse via the PFQuery
}


// Override to customize what kind of query to perform on the class. The default is to query for
// all objects ordered by createdAt descending.
- (PFQuery *)queryForTable {
    PFQuery *query = [PFQuery queryWithClassName:self.parseClassName];
    
    // If Pull To Refresh is enabled, query against the network by default.
    if (self.pullToRefreshEnabled) {
        query.cachePolicy = kPFCachePolicyNetworkOnly;
    }
    
    // If no objects are loaded in memory, we look to the cache first to fill the table
    // and then subsequently do a query against the network.
    if (self.objects.count == 0) {
        query.cachePolicy = kPFCachePolicyCacheThenNetwork;
    }
    
    [query orderByAscending:@"Start"];
    
    return query;
}



// Override to customize the look of a cell representing an object. The default is to display
// a UITableViewCellStyleDefault style cell with the label being the textKey in the object,
// and the imageView being the imageKey in the object.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath object:(PFObject *)object {
    static NSString *CellIdentifier = @"Cell";
    
    CarnivalsTableViewCell *cell = (CarnivalsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[CarnivalsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell
    //cell.textLabel.text = [object objectForKey:self.textKey];
    //cell.imageView.file = [object objectForKey:self.imageKey];
    cell.carnivalId = [object valueForKey:@"objectId"];
    [cell showStars:[[object valueForKey:@"votes"] floatValue] voters:[[object valueForKey:@"voters"] floatValue]];
    if ([object valueForKey:@"Name"]){
        [cell.cellTitle setText:[object valueForKey:@"Name"]];
    } else if([object valueForKey:@"City"]) {
        [cell.cellTitle setText:[NSString stringWithFormat:@"%@ Carnival", [object valueForKey:@"City"]]];
    } else {
        [cell.cellTitle setText:[NSString stringWithFormat:@"%@ Carnival", [object valueForKey:@"Country"]]] ;
    }
    
    if ([object valueForKey:@"State"]){
        [cell.cellSubTitle setText:[NSString stringWithFormat:@"%@, %@", [object valueForKey:@"City"], [object valueForKey:@"State"]]];
    } else {
        [cell.cellSubTitle setText:[NSString stringWithFormat:@"%@, %@", [object valueForKey:@"City"], [object valueForKey:@"Country"]]];
    }
    [cell.flagBackground setImage:[UIImage imageNamed:[NSString stringWithFormat:@"Flag of %@", [object valueForKey:@"Country"]]]];
    // NSLog(@"Date: %@", [object valueForKey:@"Start"]); => Date: 2014-12-15 00:00:00 +0000
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSLocale *enUSPOSIXLocale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormatter setLocale:enUSPOSIXLocale];
    [dateFormatter setDateFormat:@"MMM"];
    NSString *tempMonthString = [dateFormatter stringFromDate:[object valueForKey:@"Start"]];
    [dateFormatter setDateFormat:@"dd"];
    NSString *tempDayString = [dateFormatter stringFromDate:[object valueForKey:@"Start"]];
    [cell setCalendar:tempMonthString day:tempDayString];
    //NSString *formattedDateString = [dateFormatter stringFromDate:[object valueForKey:@"Start"]];
    // NSLog(@"Start Date: %@", formattedDateString); => Start Date: 2014-12-14
    return cell;
}


/*
 // Override if you need to change the ordering of objects in the table.
 - (PFObject *)objectAtIndex:(NSIndexPath *)indexPath {
 return [self.objects objectAtIndex:indexPath.row];
 }
 */


// Override to customize the look of the cell that allows the user to load the next page of objects.
// The default implementation is a UITableViewCellStyleDefault cell with simple labels.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForNextPageAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"NextPage";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.text = @"Load more...";
    
    return cell;
}


#pragma mark - UITableViewDataSource

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == self.objects.count){
        return  44.0f;
    } else {
        return 130.0f;
    }
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the object from Parse and reload the table view
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, and save it to Parse
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [super tableView:tableView didSelectRowAtIndexPath:indexPath];
    if ([_carnivalTabancaData networkAvailable]){
        CarnivalDetailsViewController *carnivalDetails = [self.storyboard instantiateViewControllerWithIdentifier:@"carnival_details_vc"];
        carnivalDetails.carnivalObject = [self.objects objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:carnivalDetails animated:YES];
        //[self performSegueWithIdentifier:@"carnivals_details_seque" sender:self];
    }
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    /*NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    CarnivalDetailsViewController *carnivalDetails = [segue destinationViewController];
    carnivalDetails.carnivalObject = [self.objects objectAtIndex:indexPath.row];*/
}

@end

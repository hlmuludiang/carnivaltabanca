//
//  DataTableViewCell.h
//  CarnivalTabanca
//
//  Created by Versatile Systems, Inc on 11/23/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DataTableViewCell : UITableViewCell {
    IBOutlet UILabel *_dataLabel;
}

-(void)setDataLabel:(NSString *)title;

@end

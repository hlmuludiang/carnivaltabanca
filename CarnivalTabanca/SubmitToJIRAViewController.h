//
//  SubmitToJIRAViewController.h
//  CarnivalTabanca
//
//  Created by Versatile Systems, Inc on 11/11/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CarnivalTabancaData.h"

@interface SubmitToJIRAViewController : UIViewController <UITextFieldDelegate, UITextViewDelegate>{
    IBOutlet UIButton *_hideKeyboardButton;
    IBOutlet UIButton *_submitToJIRA;
    IBOutlet UITextField *_description;
    IBOutlet UITextView *_details;
    CarnivalTabancaData *_carnivalTabancaData;
    UIBarButtonItem *_submitButton;
}

@property (nonatomic, retain) NSString *issueType;

@end
